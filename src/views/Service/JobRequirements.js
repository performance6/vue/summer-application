const jobrequirementservice = {
  jobrequimentperson: [
    {
      jobrequimentId: 1,
      resumeId: 1,
      jobRequirements: 'ดูแลการเงิน/การบัญชี',
      grauateDisciplines: 'การบัญชี',
      TypeJobwant: 'งาน office',
      salary: 15000.0,
      workingArea: 'กรุงเทพ',
      dateStart: '12.03.2021'
    },
    {
      workhistoryId: 2,
      resumeId: 2,
      jobRequirements: 'โปรแกรมเมอร์',
      grauateDisciplines: 'วิทยาการคอมพิวเตอร์',
      TypeJobwant: 'งาน office',
      salary: 20000.0,
      workingArea: 'ชลบุรี',
      dateStart: '9.09.2021'
    }
  ],
  lastId: 3,
  getJobReq (resumeId) {
    for (let index = 0; index < this.jobrequimentperson.length; index++) {
      if (resumeId === this.jobrequimentperson[index].resumeId) {
        return { ...this.jobrequimentperson[index] }
      } else {
        return {}
      }
    }
  }
}
export default jobrequirementservice
