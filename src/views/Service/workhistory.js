const workhistoryservice = {
  historyperson: [
    {
      workhistoryId: 1,
      resumeId: 1,
      workExerience: 1,
      nameCompany: 'summer Company',
      addressCompany: '79/2 ซอยจันทร์ 31 5 สาทร แขวงทุ่งวัดดอน กรุงเทพ 10120',
      salary: 17000.00,
      position: 'พนักงานบัญชี',
      responsibiliti: 'ดูการเงินและบัญชีภายในบริษัท',
      time: '3 ปี'
    },
    {
      workhistoryId: 2,
      resumeId: 2,
      workExerience: 1,
      nameCompany: 'summer Company',
      addressCompany: '99/109 ซอยศรีลม เมือง แขวงบ้านใต้ กาญจนบรุี 71000',
      salary: 10000.00,
      position: 'พนักงานเสิร์ฟ',
      responsibiliti: 'ดูแลบริการการจัดเตรียมเครื่องอาหารให้กับลูกค้า',
      time: '3 ปี'
    }
  ],
  lastId: 3,
  getWorkhis (resumeId) {
    for (let index = 0; index < this.historyperson.length; index++) {
      if (resumeId === this.historyperson[index].resumeId) {
        return { ...this.historyperson[index] }
      } else {
        return {}
      }
    }
  }
}
export default workhistoryservice
