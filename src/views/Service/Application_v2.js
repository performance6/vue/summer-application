const applicationService = {
  applicationList: [
    {
      id: 1,
      resumeId: 1,
      declareId: 1,
      statusApplication: 'waiting',
      statusInterview: {
        status: 'default',
        date: '',
        address: '',
        prepare: ''
      }
    },
    {
      id: 2,
      resumeId: 2,
      declareId: 1,
      statusApplication: 'waiting',
      statusInterview: {
        status: 'default',
        date: '',
        address: '',
        prepare: ''
      }
    }
  ],
  applications: [],
  getApplicationsByCompany (id) {
    this.applications = []
    for (let index = 0; index < this.applicationList.length; index++) {
      if (id === this.applicationList[index].declareId) {
        this.applications.push(this.applicationList[index])
      }
    }
    return [...this.applications]
  },
  addApplication (application) {
    this.application.id = this.lastId++
    this.applicationList.push(application)
  }
}
export default applicationService
