const educationservice = {
  educationperson: [
    {
      educationId: 1,
      resumeId: 1,
      currentState: 'กำลังศึกษาอยู่',
      leveofEducation: 'ปริญญาตรี',
      academy: 'มหาวิทยาลัยบูรพา',
      educatonal: '-',
      disciplines: 'วิทยาการคอมพิวเตอร์',
      grade: 3.00
    },
    {
      educationId: 2,
      resumeId: 2,
      currentState: 'จบการศึกษาแล้ว',
      leveofEducation: 'ปริญญาตรี',
      academy: 'มหาวิทยาลัยบูรพา',
      educatonal: 'วิทยาศาสตร์บัณฑิต',
      disciplines: 'วิทยาการคอมพิวเตอร์',
      grade: 3.50
    }
  ],
  lastId: 3,
  geteducationHistory (resumeId) {
    for (let index = 0; index < this.educationperson.length; index++) {
      if (resumeId === this.educationperson[index].resumeId) {
        return { ...this.educationperson[index] }
      } else {
        return {}
      }
    }
  }
}
export default educationservice
