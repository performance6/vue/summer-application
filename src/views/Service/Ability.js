const Ability = {
  Ability: [
    {
      id: 1,
      aboutYourself: 'สามารถใช้โปรแกรม word,excel ได้คล่อง',
      skill: 'ภาษาอังกฤษ, ภาษาจีน',
      wpm: 'ไทย 70 คำ/นาที   อังกฤษ 50 คำ/นาที'
    },
    {
      id: 2,
      aboutYourself: 'สามารถใช้โปรแกรม vi cs eclip ได้คล่อง',
      skill: 'ภาษาอังกฤษ, ภาษาเยอรมัน',
      wpm: 'ไทย 80 คำ/นาที   อังกฤษ 60 คำ/นาที'
    }
  ],
  lastId: 3,
  getAbility (id) {
    const index = this.Ability.findIndex(item => item.id === id)
    return { ...this.Ability[index] }
  }
}
export default Ability
