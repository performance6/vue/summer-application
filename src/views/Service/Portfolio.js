const portfolioService = {
  portfolioperson: [
    {
      portfolioId: 1,
      resumeId: 1,
      timeProfolio: 'ตั้งแต่ มกราคม 2564 จนถึง มกราคม 2564',
      institution: 'มหาวิทยาลัยบูรพา',
      course: 'บริหารธุรกิจบัณฑิต',
      certificate: '',
      vehicle: 'รถจักรยานยนต์',
      drivingAbility: 'รถจักรยานยนต์, รถยนต์'
    },
    {
      portfolioId: 2,
      resumeId: 2,
      timeProfolio: 'ตั้งแต่ ธันวาคม 2563 จนถึง มกราคม 2564',
      institution: 'มหาวิทยาลัยบูรพา',
      course: 'บริหารธุรกิจบัณฑิต',
      certificate: '',
      vehicle: 'รถยนต์',
      drivingAbility: 'รถจักรยานยนต์, รถยนต์'
    }
  ],
  lastId: 3,
  getportfolio (resumeId) {
    for (let index = 0; index < this.portfolioperson.length; index++) {
      if (resumeId === this.portfolioperson[index].resumeId) {
        return { ...this.portfolioperson[index] }
      } else {
        return {}
      }
    }
  }
}
export default portfolioService
